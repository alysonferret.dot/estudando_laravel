@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Cadastro de produtos</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="produto_save" method="post">
                        @csrf
                        <label>Nome produto</label>
                        <input type="text" name="nome_produto">
                        <br>
                        <br>
                        <label>quantidade</label>
                        <input type="number" name="quantidade">
                        <br>
                        <br>
                        <button>Salvar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
